package agenda.telefonica;
/**
 * @author Marcos Junior
 */
public class Reuniao {
    String titulo;
    String data;
    String mensagem;
    Reuniao(String titulo, String data, String mensagem){
        this.titulo = titulo;
        this.data = data;
        this.mensagem = mensagem;
    }
}
