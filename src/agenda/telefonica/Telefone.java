package agenda.telefonica;
/**
 * @author Marcos Junior
 */
public class Telefone {
    String operadora;
    String codigo;
    String numero;
    
    Telefone( String operadora, String codigo, String numero){
        this.operadora = operadora;
        this.codigo = codigo;
        this.numero = numero;
    }
}
