package agenda.telefonica;
/**
 * @author Marcos Junior
 */
import java.util.ArrayList;
import java.util.List;

public class Contato {

    String nome, grupo;
    String empresa, setor, parentesco;
    String pais, estado, cidade, bairro, rua, numero, cep, complemento;
    String dia,mes,ano;

    List<Telefone> listaDeTelefones;
    List<Reuniao> listaDeReunioes;
    List<String> listaDeEmails;

    Contato() {
        listaDeTelefones = new ArrayList<>();
        listaDeReunioes = new ArrayList<>();
        listaDeEmails = new ArrayList<>();
    }

    public void setAniversario(String dia, String mes, String ano) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }

    public void setEndereco(String Pais, String Estado, String Cidade,
            String Bairro, String Rua, String Numero,
            String CEP, String Complemento) {
        pais = Pais;
        estado = Estado;
        cidade = Cidade;
        bairro = Bairro;
        rua = Rua;
        numero = Numero;
        cep = CEP;
        complemento = Complemento;
    }

    public void addTelefone(Telefone tel) {
        listaDeTelefones.add(tel);
    }
    public void removeTelefone(int indice) {
        listaDeTelefones.remove(indice);
    }

    public void addReuniao(Reuniao reuniao) {
        listaDeReunioes.add(reuniao);
    }
    public void removeReuniao(int indice) {
        listaDeReunioes.remove(indice);
    }

    public void addEmail(String email) {
        listaDeEmails.add(email);
    }
    public void removeEmail(int indice) {
        listaDeEmails.remove(indice);
    }
    
}
