package agenda.telefonica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractListModel;
import javax.swing.JOptionPane;

/**
 * @author Marcos Junior
 */
public class TelaPrincipal extends javax.swing.JFrame {

    List<Contato> contatosList = new ArrayList<>();
    String[] contatosString;
    JavaDB BancoDeDados;
    //----------------------------------------------------------------------------------
    int indiceJLReunioes;
    //**indiceJLReunioes->necessario pra atualizar o indice da lista reuniões, porque nao da pra setar
    // o indiceJLReunioes de dentro do item changed, e perde qual evendo foi selecionado depois de 
    //mudar o model da lista
    //-----------------------------------------------------------------------------------

    String funcao;

    public TelaPrincipal() {
        initComponents();
        BancoDeDados = new JavaDB();
        FrameAdicionarContato.setLocation(jLabel3.getLocation());
        atualizaContatos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        FrameAdicionarContato = new javax.swing.JFrame();
        jLabel2 = new javax.swing.JLabel();
        addcontatoETnome = new javax.swing.JTextField();
        addcontatoCBgrupo = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        addcontatoCBdia = new javax.swing.JComboBox<>();
        addcontatoCBmes = new javax.swing.JComboBox<>();
        addcontatoCBano = new javax.swing.JComboBox<>();
        addcontatoETpais = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        addcontatoETestado = new javax.swing.JTextField();
        addcontatoETcidade = new javax.swing.JTextField();
        addcontatoETbairro = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        addcontatoETrua = new javax.swing.JTextField();
        addcontatoETnumero = new javax.swing.JTextField();
        addcontatoETcomplemento = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        LBaddcontatoEmpresa = new javax.swing.JLabel();
        addcontatoETempresa_parentesco = new javax.swing.JTextField();
        addcontatoETsetor = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        addcontatoBTsalvar = new javax.swing.JButton();
        addcontatoETcep = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        FrameAdicionarTelefone = new javax.swing.JFrame();
        addtelefoneCBoperadora = new javax.swing.JComboBox<>();
        addtelefoneETcodigo = new javax.swing.JTextField();
        addtelefoneETnumero = new javax.swing.JTextField();
        addtelefoneBTadicionar = new javax.swing.JButton();
        FrameAdicionarReuniao = new javax.swing.JFrame();
        jLabel7 = new javax.swing.JLabel();
        addreuniaoETtitulo = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        addreuniaoCBdia = new javax.swing.JComboBox<>();
        addreuniaoCBmes = new javax.swing.JComboBox<>();
        addreuniaoCBano = new javax.swing.JComboBox<>();
        addreuniaoBTsalvar = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        addreuniaoETdescricao = new javax.swing.JTextArea();
        FrameAdicionarEmail = new javax.swing.JFrame();
        jLabel16 = new javax.swing.JLabel();
        adicionaremailETemail = new javax.swing.JTextField();
        adicionaremailBTsalvar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JLcontatos = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        LBnomeContato = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        LBendereco1 = new javax.swing.JLabel();
        LBendereco2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        BTadicionarContato = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        LBdataNascimento = new javax.swing.JLabel();
        LBaniversario1 = new javax.swing.JLabel();
        LBcontaDias = new javax.swing.JLabel();
        LBaniversario2 = new javax.swing.JLabel();
        LBidade = new javax.swing.JLabel();
        LBaniversario3 = new javax.swing.JLabel();
        BTeditarContato = new javax.swing.JButton();
        BTexcluirContato = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        LBgrupo = new javax.swing.JLabel();
        LBinfo1 = new javax.swing.JLabel();
        LBinfo2 = new javax.swing.JLabel();
        BTadicionarReuniao = new javax.swing.JButton();
        CBmodoExibicao = new javax.swing.JComboBox<>();
        BTadicionarTelefone = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        JLtelefones = new javax.swing.JList<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        JLreunioes = new javax.swing.JList<>();
        BTexcluirTelefone = new javax.swing.JButton();
        BTeditarTelefone = new javax.swing.JButton();
        BTexcluirReuniao = new javax.swing.JButton();
        BTeditarReuniao = new javax.swing.JButton();
        ETbusca = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        JLemails = new javax.swing.JList<>();
        BTeditarEmail = new javax.swing.JButton();
        BTexcluirEmail = new javax.swing.JButton();
        BTadicionarEmail = new javax.swing.JButton();

        FrameAdicionarContato.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        FrameAdicionarContato.setTitle("Adicionar Contato");
        FrameAdicionarContato.setMinimumSize(new java.awt.Dimension(235, 300));
        FrameAdicionarContato.setPreferredSize(new java.awt.Dimension(220, 300));
        FrameAdicionarContato.setResizable(false);
        FrameAdicionarContato.setSize(new java.awt.Dimension(220, 329));
        FrameAdicionarContato.setType(java.awt.Window.Type.UTILITY);

        jLabel2.setText("Nome:");

        addcontatoCBgrupo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Amigo", "Família", "Trabalho" }));
        addcontatoCBgrupo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                addcontatoCBgrupoItemStateChanged(evt);
            }
        });

        jLabel6.setText("Grupo:");

        jLabel8.setText("Nascimento:");

        addcontatoCBdia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        addcontatoCBmes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));

        addcontatoCBano.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017" }));

        jLabel10.setText("País:");

        jLabel14.setText("Estado:");

        jLabel15.setText("Cidade:");

        jLabel17.setText("Bairro:");

        jLabel18.setText("Rua:");

        jLabel19.setText("Numero:");

        jLabel20.setText("Complemento:");

        LBaddcontatoEmpresa.setText("Empresa:");

        jLabel22.setText("Setor:");

        addcontatoBTsalvar.setText("Salvar");
        addcontatoBTsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addcontatoBTsalvarActionPerformed(evt);
            }
        });

        jLabel21.setText("CEP:");

        javax.swing.GroupLayout FrameAdicionarContatoLayout = new javax.swing.GroupLayout(FrameAdicionarContato.getContentPane());
        FrameAdicionarContato.getContentPane().setLayout(FrameAdicionarContatoLayout);
        FrameAdicionarContatoLayout.setHorizontalGroup(
            FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETsetor, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(LBaddcontatoEmpresa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoETempresa_parentesco))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoBTsalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoETcep))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETcomplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETrua, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETbairro, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETcidade, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETestado, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addcontatoETpais, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(6, 6, 6)
                        .addComponent(addcontatoETnome, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBdia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addcontatoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        FrameAdicionarContatoLayout.setVerticalGroup(
            FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarContatoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(addcontatoETnome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(addcontatoCBdia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addcontatoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addcontatoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETpais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETcidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETbairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETrua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETcomplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETcep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoBTsalvar)
                    .addComponent(addcontatoCBgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETempresa_parentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LBaddcontatoEmpresa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarContatoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addcontatoETsetor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        FrameAdicionarContato.getAccessibleContext().setAccessibleParent(BTadicionarContato);

        FrameAdicionarTelefone.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        FrameAdicionarTelefone.setTitle("Adicionar Telefone");
        FrameAdicionarTelefone.setMinimumSize(new java.awt.Dimension(241, 100));
        FrameAdicionarTelefone.setResizable(false);
        FrameAdicionarTelefone.setSize(new java.awt.Dimension(241, 100));
        FrameAdicionarTelefone.setType(java.awt.Window.Type.UTILITY);

        addtelefoneCBoperadora.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TIM", "VIVO", "OI", "CLARO", "GVT", "NEXTEL" }));

        addtelefoneBTadicionar.setText("Adicionar");
        addtelefoneBTadicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addtelefoneBTadicionarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FrameAdicionarTelefoneLayout = new javax.swing.GroupLayout(FrameAdicionarTelefone.getContentPane());
        FrameAdicionarTelefone.getContentPane().setLayout(FrameAdicionarTelefoneLayout);
        FrameAdicionarTelefoneLayout.setHorizontalGroup(
            FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarTelefoneLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FrameAdicionarTelefoneLayout.createSequentialGroup()
                        .addComponent(addtelefoneCBoperadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(addtelefoneETcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(addtelefoneETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(addtelefoneBTadicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FrameAdicionarTelefoneLayout.setVerticalGroup(
            FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarTelefoneLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(FrameAdicionarTelefoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addtelefoneCBoperadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addtelefoneETcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addtelefoneETnumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(addtelefoneBTadicionar)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        FrameAdicionarReuniao.setTitle("Salvar Reunião");
        FrameAdicionarReuniao.setMinimumSize(new java.awt.Dimension(250, 210));
        FrameAdicionarReuniao.setResizable(false);
        FrameAdicionarReuniao.setSize(new java.awt.Dimension(250, 210));
        FrameAdicionarReuniao.setType(java.awt.Window.Type.UTILITY);

        jLabel7.setText("Titulo:");

        jLabel9.setText("Data:");

        addreuniaoCBdia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        addreuniaoCBmes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));

        addreuniaoCBano.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032" }));

        addreuniaoBTsalvar.setText("Salvar");
        addreuniaoBTsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addreuniaoBTsalvarActionPerformed(evt);
            }
        });

        addreuniaoETdescricao.setColumns(20);
        addreuniaoETdescricao.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        addreuniaoETdescricao.setRows(5);
        addreuniaoETdescricao.setText("Descrição:");
        jScrollPane6.setViewportView(addreuniaoETdescricao);

        javax.swing.GroupLayout FrameAdicionarReuniaoLayout = new javax.swing.GroupLayout(FrameAdicionarReuniao.getContentPane());
        FrameAdicionarReuniao.getContentPane().setLayout(FrameAdicionarReuniaoLayout);
        FrameAdicionarReuniaoLayout.setHorizontalGroup(
            FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                        .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                                .addComponent(addreuniaoCBdia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addreuniaoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addreuniaoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(addreuniaoETtitulo)))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FrameAdicionarReuniaoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(addreuniaoBTsalvar)))
                .addContainerGap())
        );
        FrameAdicionarReuniaoLayout.setVerticalGroup(
            FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarReuniaoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(addreuniaoETtitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FrameAdicionarReuniaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addreuniaoCBdia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(addreuniaoCBmes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addreuniaoCBano, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addreuniaoBTsalvar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        FrameAdicionarEmail.setTitle("Adicionar Email");
        FrameAdicionarEmail.setMinimumSize(new java.awt.Dimension(311, 65));
        FrameAdicionarEmail.setPreferredSize(new java.awt.Dimension(311, 65));
        FrameAdicionarEmail.setResizable(false);
        FrameAdicionarEmail.setSize(new java.awt.Dimension(311, 65));

        jLabel16.setText("Email:");

        adicionaremailBTsalvar.setText("Salvar");
        adicionaremailBTsalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionaremailBTsalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FrameAdicionarEmailLayout = new javax.swing.GroupLayout(FrameAdicionarEmail.getContentPane());
        FrameAdicionarEmail.getContentPane().setLayout(FrameAdicionarEmailLayout);
        FrameAdicionarEmailLayout.setHorizontalGroup(
            FrameAdicionarEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarEmailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adicionaremailETemail, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adicionaremailBTsalvar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FrameAdicionarEmailLayout.setVerticalGroup(
            FrameAdicionarEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FrameAdicionarEmailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FrameAdicionarEmailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(adicionaremailETemail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(adicionaremailBTsalvar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(744, 565));
        setSize(new java.awt.Dimension(744, 565));

        JLcontatos.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Sem contatos..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLcontatos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLcontatosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(JLcontatos);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("CONTATOS");

        LBnomeContato.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LBnomeContato.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LBnomeContato.setText("Contato");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_telefones.png"))); // NOI18N
        jLabel3.setText("Telefones:");

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_contatos.png"))); // NOI18N

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_contato.png"))); // NOI18N

        LBendereco1.setText("Rua - N° 000 - Bairro - Complemento");

        LBendereco2.setText("Cidade - Estado - País - CEP");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_endereco.png"))); // NOI18N
        jLabel4.setText("Endereço:");

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        BTadicionarContato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarcontato.png"))); // NOI18N
        BTadicionarContato.setText("Adicionar Contato");
        BTadicionarContato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarContatoActionPerformed(evt);
            }
        });

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_aniversario.png"))); // NOI18N
        jLabel5.setText("Aniversário:");

        LBcontaDias.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        BTeditarContato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editarcontato.png"))); // NOI18N
        BTeditarContato.setToolTipText("Editar Contato");
        BTeditarContato.setEnabled(false);
        BTeditarContato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarContatoActionPerformed(evt);
            }
        });

        BTexcluirContato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_excluircontato.png"))); // NOI18N
        BTexcluirContato.setToolTipText("Excluir Contato");
        BTexcluirContato.setEnabled(false);
        BTexcluirContato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirContatoActionPerformed(evt);
            }
        });

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_familia.png"))); // NOI18N
        LBgrupo.setText("Família");

        BTadicionarReuniao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarreuniao.png"))); // NOI18N
        BTadicionarReuniao.setToolTipText("Adicionar Reunião");
        BTadicionarReuniao.setEnabled(false);
        BTadicionarReuniao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarReuniaoActionPerformed(evt);
            }
        });

        CBmodoExibicao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Exibir Todos", "Exibir Família", "Exibir Amigos", "Exibir Trabalho" }));
        CBmodoExibicao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CBmodoExibicaoItemStateChanged(evt);
            }
        });

        BTadicionarTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarreuniao.png"))); // NOI18N
        BTadicionarTelefone.setToolTipText("Adicionar Telefone");
        BTadicionarTelefone.setEnabled(false);
        BTadicionarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarTelefoneActionPerformed(evt);
            }
        });

        JLtelefones.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Nenhum telefone salvo..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLtelefones.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLtelefonesValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(JLtelefones);

        JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Nenhuma reunião cadastrada..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLreunioes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                JLreunioesMouseReleased(evt);
            }
        });
        JLreunioes.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLreunioesValueChanged(evt);
            }
        });
        jScrollPane5.setViewportView(JLreunioes);

        BTexcluirTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_deletarreuniao.png"))); // NOI18N
        BTexcluirTelefone.setToolTipText("Adicionar Reunião");
        BTexcluirTelefone.setEnabled(false);
        BTexcluirTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirTelefoneActionPerformed(evt);
            }
        });

        BTeditarTelefone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editar.png"))); // NOI18N
        BTeditarTelefone.setToolTipText("Adicionar Reunião");
        BTeditarTelefone.setEnabled(false);
        BTeditarTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarTelefoneActionPerformed(evt);
            }
        });

        BTexcluirReuniao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_deletarreuniao.png"))); // NOI18N
        BTexcluirReuniao.setToolTipText("Adicionar Reunião");
        BTexcluirReuniao.setEnabled(false);
        BTexcluirReuniao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirReuniaoActionPerformed(evt);
            }
        });

        BTeditarReuniao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editar.png"))); // NOI18N
        BTeditarReuniao.setToolTipText("Adicionar Reunião");
        BTeditarReuniao.setEnabled(false);
        BTeditarReuniao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarReuniaoActionPerformed(evt);
            }
        });

        ETbusca.setText("Busca...");
        ETbusca.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ETbuscaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                ETbuscaFocusLost(evt);
            }
        });
        ETbusca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ETbuscaKeyReleased(evt);
            }
        });

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_email.png"))); // NOI18N
        jLabel11.setText("Emails:");

        JLemails.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Nenhum email cadastrado..." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        JLemails.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                JLemailsValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(JLemails);

        BTeditarEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_editar.png"))); // NOI18N
        BTeditarEmail.setEnabled(false);
        BTeditarEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTeditarEmailActionPerformed(evt);
            }
        });

        BTexcluirEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_deletarreuniao.png"))); // NOI18N
        BTexcluirEmail.setEnabled(false);
        BTexcluirEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTexcluirEmailActionPerformed(evt);
            }
        });

        BTadicionarEmail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_adicionarreuniao.png"))); // NOI18N
        BTadicionarEmail.setEnabled(false);
        BTadicionarEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTadicionarEmailActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(CBmodoExibicao, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETbusca, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BTadicionarContato, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LBnomeContato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTeditarContato)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTexcluirContato))
                    .addComponent(jSeparator2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTadicionarTelefone)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTeditarTelefone)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTexcluirTelefone))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel11)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BTadicionarEmail)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(BTeditarEmail)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(BTexcluirEmail))))
                        .addGap(20, 20, 20)
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5)
                            .addComponent(jSeparator3)
                            .addComponent(LBinfo2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LBinfo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(LBgrupo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                                .addComponent(BTadicionarReuniao)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTeditarReuniao)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(BTexcluirReuniao))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(LBaniversario1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBcontaDias)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBaniversario2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBidade)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBaniversario3))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBdataNascimento)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jSeparator4)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(LBendereco1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LBendereco2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ETbusca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CBmodoExibicao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BTadicionarContato))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(BTeditarContato)
                                .addComponent(BTexcluirContato))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(LBnomeContato, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel3)
                                        .addComponent(BTeditarTelefone, javax.swing.GroupLayout.Alignment.TRAILING))
                                    .addComponent(BTadicionarTelefone)
                                    .addComponent(BTexcluirReuniao)
                                    .addComponent(BTeditarReuniao)
                                    .addComponent(BTadicionarReuniao)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(LBgrupo)
                                        .addComponent(BTexcluirTelefone)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBinfo1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(LBinfo2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel5)
                                            .addComponent(LBdataNascimento))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(LBaniversario1)
                                            .addComponent(LBcontaDias)
                                            .addComponent(LBaniversario2)
                                            .addComponent(LBidade)
                                            .addComponent(LBaniversario3)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(BTexcluirEmail, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(BTeditarEmail, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(BTadicionarEmail, javax.swing.GroupLayout.Alignment.TRAILING))
                                        .addGap(13, 13, 13)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jSeparator5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LBendereco1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LBendereco2)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

//SELETOR DE EXIBIÇÂO----------------------------------------------------------------------------
    private void CBmodoExibicaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CBmodoExibicaoItemStateChanged
        atualizaContatos();
        limpaTela();
    }//GEN-LAST:event_CBmodoExibicaoItemStateChanged
//SELETOR DE EXIBIÇÂO----------------------------------------------------------------------------

//EVENTOS DAS LISTAS-----------------------------------------------------------------------------
    private void JLreunioesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLreunioesValueChanged
        //int indiceJLReunioes = -1;
        if (evt.getValueIsAdjusting()) {
            if ((JLcontatos.getSelectedIndex() != -1) && (JLreunioes.getSelectedIndex() != -1)) {
                indiceJLReunioes = JLreunioes.getSelectedIndex();
                String titulo = contatosList.get(JLcontatos.getSelectedIndex()).listaDeReunioes.get(JLreunioes.getSelectedIndex()).titulo;
                String data = contatosList.get(JLcontatos.getSelectedIndex()).listaDeReunioes.get(JLreunioes.getSelectedIndex()).data;
                String mensagem = contatosList.get(JLcontatos.getSelectedIndex()).listaDeReunioes.get(JLreunioes.getSelectedIndex()).mensagem;

                Contato contato = contatosList.get(JLcontatos.getSelectedIndex());
                String reunioes[] = new String[contato.listaDeReunioes.size()];
                for (int i = 0; i < contato.listaDeReunioes.size(); i++) {
                    if (i == JLreunioes.getSelectedIndex()) {
                        reunioes[i] = "<HTML>" + titulo + " - (" + data + ")<BR>" + mensagem.replace("\n", "<BR>");
                    } else {
                        reunioes[i] = contato.listaDeReunioes.get(i).titulo + " - ("
                                + contato.listaDeReunioes.get(i).data + ")";
                    }
                }
                JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
                    public int getSize() {
                        return reunioes.length;
                    }

                    public String getElementAt(int i) {
                        return reunioes[i];
                    }
                });
                BTeditarReuniao.setEnabled(true);
                BTexcluirReuniao.setEnabled(true);
            } else {
                BTeditarReuniao.setEnabled(false);
                BTexcluirReuniao.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLreunioesValueChanged
    private void JLtelefonesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLtelefonesValueChanged
        if (evt.getValueIsAdjusting()) {
            if ((JLcontatos.getSelectedIndex() != -1) && (JLtelefones.getSelectedIndex() != -1)) {
                BTeditarTelefone.setEnabled(true);
                BTexcluirTelefone.setEnabled(true);
            } else {
                BTeditarTelefone.setEnabled(false);
                BTexcluirTelefone.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLtelefonesValueChanged
    private void JLcontatosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLcontatosValueChanged
        if (evt.getValueIsAdjusting()) {
            if (JLcontatos.getSelectedIndex() != -1) {
                indiceJLReunioes = 0;
                indiceJLReunioes = 0;
                atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
                BTeditarContato.setEnabled(true);
                BTexcluirContato.setEnabled(true);

                BTadicionarTelefone.setEnabled(true);
                BTeditarTelefone.setEnabled(false);
                BTexcluirTelefone.setEnabled(false);
                BTadicionarReuniao.setEnabled(true);
                BTeditarReuniao.setEnabled(false);
                BTexcluirReuniao.setEnabled(false);
                BTadicionarEmail.setEnabled(true);
                BTeditarEmail.setEnabled(false);
                BTexcluirEmail.setEnabled(false);
            } else {
                BTeditarContato.setEnabled(false);
                BTexcluirContato.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLcontatosValueChanged
    private void JLreunioesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JLreunioesMouseReleased
        JLreunioes.setSelectedIndex(indiceJLReunioes);
    }//GEN-LAST:event_JLreunioesMouseReleased
//EVENTOS DAS LISTAS-----------------------------------------------------------------------------

//BOTOES DE CONTATO------------------------------------------------------------------------------
    private void BTadicionarContatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarContatoActionPerformed
        funcao = "adicionar";
        FrameAdicionarContato.setTitle("Adicionar Contato");
        FrameAdicionarContato.setVisible(true);
    }//GEN-LAST:event_BTadicionarContatoActionPerformed
    private void BTeditarContatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarContatoActionPerformed
        funcao = "editar";
        Contato contato = contatosList.get(JLcontatos.getSelectedIndex());

        FrameAdicionarContato.setTitle("Editar Contato");
        addcontatoETnome.setText(contato.nome);
        addcontatoCBdia.setSelectedItem(contato.dia);
        addcontatoCBmes.setSelectedItem(contato.mes);
        addcontatoCBano.setSelectedItem(contato.ano);
        addcontatoETpais.setText(contato.pais);
        addcontatoETestado.setText(contato.estado);
        addcontatoETcidade.setText(contato.cidade);
        addcontatoETbairro.setText(contato.bairro);
        addcontatoETrua.setText(contato.rua);
        addcontatoETnumero.setText(contato.numero);
        addcontatoETcomplemento.setText(contato.complemento);
        addcontatoETcep.setText(contato.cep);
        addcontatoCBgrupo.setSelectedItem(contato.grupo);
        if (contato.grupo.equals("Amigo")) {
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 329);//Amigo
        }
        if (contato.grupo.equals("Família")) {
            LBaddcontatoEmpresa.setText("Parentesco:");
            addcontatoETempresa_parentesco.setText(contato.parentesco);
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 354);//Família
        }
        if (contato.grupo.equals("Trabalho")) {
            LBaddcontatoEmpresa.setText("Empresa:");
            addcontatoETempresa_parentesco.setText(contato.empresa);
            addcontatoETsetor.setText(contato.setor);
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 384);//Trabalho
        }
        FrameAdicionarContato.setVisible(true);
    }//GEN-LAST:event_BTeditarContatoActionPerformed
    private void BTexcluirContatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirContatoActionPerformed
        BancoDeDados.excluirContato(contatosList.get(JLcontatos.getSelectedIndex()));
        atualizaContatos();
        limpaTela();
        BTexcluirContato.setEnabled(false);
        BTeditarContato.setEnabled(false);
        BTadicionarTelefone.setEnabled(false);
        BTadicionarReuniao.setEnabled(false);
    }//GEN-LAST:event_BTexcluirContatoActionPerformed
//BOTOES DE CONTATO------------------------------------------------------------------------------

//BOTOES DE TELEFONE-----------------------------------------------------------------------------
    private void BTadicionarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarTelefoneActionPerformed
        funcao = "adicionar";
        addtelefoneCBoperadora.setSelectedIndex(0);
        addtelefoneETcodigo.setText("");
        addtelefoneETnumero.setText("");
        addtelefoneBTadicionar.setText("Adicionar");
        FrameAdicionarTelefone.setTitle("Adicionar Telefone");
        FrameAdicionarTelefone.setVisible(true);
    }//GEN-LAST:event_BTadicionarTelefoneActionPerformed
    private void BTeditarTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarTelefoneActionPerformed
        funcao = "editar";
        String operadora = contatosList.get(JLcontatos.getSelectedIndex()).listaDeTelefones.get(JLtelefones.getSelectedIndex()).operadora;
        String codigo = contatosList.get(JLcontatos.getSelectedIndex()).listaDeTelefones.get(JLtelefones.getSelectedIndex()).codigo;
        String numero = contatosList.get(JLcontatos.getSelectedIndex()).listaDeTelefones.get(JLtelefones.getSelectedIndex()).numero;
        addtelefoneCBoperadora.setSelectedItem(operadora);
        addtelefoneETcodigo.setText(codigo);
        addtelefoneETnumero.setText(numero);
        addtelefoneBTadicionar.setText("Salvar");
        FrameAdicionarTelefone.setTitle("Editar Telefone");
        FrameAdicionarTelefone.setVisible(true);

    }//GEN-LAST:event_BTeditarTelefoneActionPerformed
    private void BTexcluirTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirTelefoneActionPerformed
        contatosList.get(JLcontatos.getSelectedIndex()).removeTelefone(JLtelefones.getSelectedIndex());
        BancoDeDados.atualizarTelefones(contatosList.get(JLcontatos.getSelectedIndex()));
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
        BTexcluirTelefone.setEnabled(false);
        BTeditarTelefone.setEnabled(false);
    }//GEN-LAST:event_BTexcluirTelefoneActionPerformed
//BOTOES DE TELEFONE-----------------------------------------------------------------------------

//BOTOES DE REUNIAO------------------------------------------------------------------------------    
    private void BTadicionarReuniaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarReuniaoActionPerformed
        funcao = "adicionar";
        addreuniaoETtitulo.setText("");
        Calendar calendario = new GregorianCalendar();
        calendario.setTime(new Date());
        String ano = Integer.toString(calendario.get(Calendar.YEAR));
        addreuniaoCBdia.setSelectedIndex(calendario.get(Calendar.DATE) - 1);
        addreuniaoCBmes.setSelectedIndex(calendario.get(Calendar.MONTH));
        addreuniaoCBano.setSelectedItem(ano);
        addreuniaoETdescricao.setText("");
        addreuniaoBTsalvar.setText("Adicionar");
        FrameAdicionarReuniao.setTitle("Adicionar Reuniao");
        FrameAdicionarReuniao.setVisible(true);
    }//GEN-LAST:event_BTadicionarReuniaoActionPerformed
    private void BTeditarReuniaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarReuniaoActionPerformed
        funcao = "editar";
        addreuniaoETtitulo.setText(contatosList.get(JLcontatos.getSelectedIndex()).listaDeReunioes.get(JLreunioes.getSelectedIndex()).titulo);
        Date data;
        String ano = "1970";
        Calendar calendario = new GregorianCalendar();
        SimpleDateFormat dataFormater = new SimpleDateFormat("dd/MM/yyyy");
        try {
            data = dataFormater.parse(contatosList.get(JLcontatos.getSelectedIndex()).listaDeReunioes.get(JLreunioes.getSelectedIndex()).data);
            calendario.setTime(data);
            ano = Integer.toString(calendario.get(Calendar.YEAR));
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        addreuniaoCBdia.setSelectedIndex(calendario.get(Calendar.DATE) - 1);
        addreuniaoCBmes.setSelectedIndex(calendario.get(Calendar.MONTH));
        addreuniaoCBano.setSelectedItem(ano);
        addreuniaoETdescricao.setText(contatosList.get(JLcontatos.getSelectedIndex()).listaDeReunioes.get(JLreunioes.getSelectedIndex()).mensagem);
        addreuniaoBTsalvar.setText("Editar");
        FrameAdicionarReuniao.setTitle("Editar Reuniao");
        FrameAdicionarReuniao.setVisible(true);
    }//GEN-LAST:event_BTeditarReuniaoActionPerformed
    private void BTexcluirReuniaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirReuniaoActionPerformed
        contatosList.get(JLcontatos.getSelectedIndex()).removeReuniao(JLreunioes.getSelectedIndex());
        BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
        BTeditarReuniao.setEnabled(false);
        BTexcluirReuniao.setEnabled(false);
    }//GEN-LAST:event_BTexcluirReuniaoActionPerformed
//BOTOES DE REUNIAO------------------------------------------------------------------------------

//FRAME ADD CONTATO------------------------------------------------------------------------------    
    private void addcontatoCBgrupoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_addcontatoCBgrupoItemStateChanged
        if (addcontatoCBgrupo.getSelectedIndex() == 0) {
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 329);//Amigo
        }
        if (addcontatoCBgrupo.getSelectedIndex() == 1) {
            LBaddcontatoEmpresa.setText("Parentesco:");
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 354);//Família
        }
        if (addcontatoCBgrupo.getSelectedIndex() == 2) {
            LBaddcontatoEmpresa.setText("Empresa:");
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 384);//Trabalho
        }
    }//GEN-LAST:event_addcontatoCBgrupoItemStateChanged
    private void addcontatoBTsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addcontatoBTsalvarActionPerformed
        if (addcontatoETnome.getText().trim().equals("")
                || addcontatoETpais.getText().trim().equals("")
                || addcontatoETestado.getText().trim().equals("")
                || addcontatoETcidade.getText().trim().equals("")
                || addcontatoETbairro.getText().trim().equals("")
                || addcontatoETrua.getText().trim().equals("")
                || addcontatoETnumero.getText().trim().equals("")
                || addcontatoETcomplemento.getText().trim().equals("")
                || addcontatoETcep.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Por favor preencha todos os campos!!",
                    "Campos não preenchidos corretamente", JOptionPane.ERROR_MESSAGE);
        } else {
            Contato contato = new Contato();
            contato.nome = addcontatoETnome.getText();
            contato.setAniversario(addcontatoCBdia.getSelectedItem().toString(),
                    addcontatoCBmes.getSelectedItem().toString(),
                    addcontatoCBano.getSelectedItem().toString());

            contato.setEndereco(addcontatoETpais.getText(),
                    addcontatoETestado.getText(),
                    addcontatoETcidade.getText(),
                    addcontatoETbairro.getText(),
                    addcontatoETrua.getText(),
                    addcontatoETnumero.getText(),
                    addcontatoETcep.getText(),
                    addcontatoETcomplemento.getText());

            contato.grupo = addcontatoCBgrupo.getSelectedItem().toString();

            if (contato.grupo.equals("Amigo")) {
                contato.parentesco = "";
                contato.empresa = "";
                contato.setor = "";
            }
            if (contato.grupo.equals("Família")) {
                contato.parentesco = addcontatoETempresa_parentesco.getText();
                contato.empresa = "";
                contato.setor = "";
            }
            if (contato.grupo.equals("Trabalho")) {
                contato.parentesco = "";
                contato.empresa = addcontatoETempresa_parentesco.getText();
                contato.setor = addcontatoETsetor.getText();
            }
            if (funcao.equals("editar")) {
                BancoDeDados.atualizarContato(contatosList.get(JLcontatos.getSelectedIndex()), contato);
            }
            if (funcao.equals("adicionar")) {
                BancoDeDados.inserirContato(contato);
            }
            atualizaContatos();

            addcontatoETnome.setText("");
            addcontatoCBdia.setSelectedIndex(0);
            addcontatoCBmes.setSelectedIndex(0);
            addcontatoCBano.setSelectedIndex(0);
            addcontatoETpais.setText("");
            addcontatoETestado.setText("");
            addcontatoETcidade.setText("");
            addcontatoETbairro.setText("");
            addcontatoETrua.setText("");
            addcontatoETnumero.setText("");
            addcontatoETcomplemento.setText("");
            addcontatoETcep.setText("");
            addcontatoCBgrupo.setSelectedIndex(0);
            FrameAdicionarContato.setSize(FrameAdicionarContato.getWidth(), 329);//Amigo
            addcontatoETempresa_parentesco.setText("");
            addcontatoETsetor.setText("");
            FrameAdicionarContato.setVisible(false);
        }
    }//GEN-LAST:event_addcontatoBTsalvarActionPerformed
//FRAME ADD CONTATO------------------------------------------------------------------------------

//FRAME ADD TELEFONE-----------------------------------------------------------------------------    
    private void addtelefoneBTadicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addtelefoneBTadicionarActionPerformed
        Telefone tel = new Telefone( //Cria instância de Telefone
                addtelefoneCBoperadora.getSelectedItem().toString(),//
                addtelefoneETcodigo.getText(), //
                addtelefoneETnumero.getText());                     //

        if (funcao.equals("adicionar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).addTelefone(tel);// adiciona ela no contato selecionado
            BancoDeDados.atualizarTelefones(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }
        if (funcao.equals("editar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).listaDeTelefones.remove(JLtelefones.getSelectedIndex());
            contatosList.get(JLcontatos.getSelectedIndex()).addTelefone(tel);// adiciona ela no contato selecionado
            BancoDeDados.atualizarTelefones(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }

        FrameAdicionarTelefone.dispose();
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza a tela
    }//GEN-LAST:event_addtelefoneBTadicionarActionPerformed
//FRAME ADD TELEFONE-----------------------------------------------------------------------------

//FRAME ADD REUNIAO-------------------------------------------------------------------------------    
    private void addreuniaoBTsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addreuniaoBTsalvarActionPerformed
        Reuniao reuniao = new Reuniao(
                addreuniaoETtitulo.getText(),
                addreuniaoCBdia.getSelectedItem().toString() + "/"
                + addreuniaoCBmes.getSelectedItem().toString() + "/"
                + addreuniaoCBano.getSelectedItem().toString(),
                addreuniaoETdescricao.getText()
        );

        if (funcao.equals("adicionar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).addReuniao(reuniao);
            BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
        }
        if (funcao.equals("editar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).listaDeReunioes.remove(JLreunioes.getSelectedIndex());
            contatosList.get(JLcontatos.getSelectedIndex()).addReuniao(reuniao);
            BancoDeDados.atualizarReunioes(contatosList.get(JLcontatos.getSelectedIndex()));
        }

        FrameAdicionarReuniao.dispose();
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
    }//GEN-LAST:event_addreuniaoBTsalvarActionPerformed
//FRAME ADD REUNIAO-------------------------------------------------------------------------------

//PESQUISAR CONTATO------------------------------------------------------------------------------   
    private void ETbuscaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ETbuscaKeyReleased
        contatosList = BancoDeDados.pesquisarContatos(CBmodoExibicao.getSelectedIndex(), ETbusca.getText());
        contatosString = new String[contatosList.size()];
        for (int i = 0; i < contatosList.size(); i++) {
            contatosString[i] = contatosList.get(i).nome;
        }
        JLcontatos.setModel(new AbstractListModel<String>() {
            public int getSize() {
                return contatosString.length;
            }

            public String getElementAt(int i) {
                return contatosString[i];
            }
        });
        limpaTela();
    }//GEN-LAST:event_ETbuscaKeyReleased
    private void ETbuscaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ETbuscaFocusGained
        ETbusca.setText("");
    }//GEN-LAST:event_ETbuscaFocusGained
    private void ETbuscaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ETbuscaFocusLost
        ETbusca.setText("Buscar...");
    }//GEN-LAST:event_ETbuscaFocusLost
//PESQUISAR CONTATO------------------------------------------------------------------------------

//BOTOES DE EMAIL-----------------------------------------------------------------------------
    private void BTadicionarEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTadicionarEmailActionPerformed
        funcao = "adicionar";
        FrameAdicionarEmail.setTitle("Adicionar Email");
        FrameAdicionarEmail.setVisible(true);
    }//GEN-LAST:event_BTadicionarEmailActionPerformed
    private void BTeditarEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTeditarEmailActionPerformed
        funcao = "editar";
        FrameAdicionarEmail.setTitle("Editar Email");
        FrameAdicionarEmail.setVisible(true);
    }//GEN-LAST:event_BTeditarEmailActionPerformed
    private void BTexcluirEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTexcluirEmailActionPerformed
        contatosList.get(JLcontatos.getSelectedIndex()).removeEmail(JLemails.getSelectedIndex());
        BancoDeDados.atualizarEmails(contatosList.get(JLcontatos.getSelectedIndex()));
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));
        BTeditarEmail.setEnabled(false);
        BTexcluirEmail.setEnabled(false);
    }//GEN-LAST:event_BTexcluirEmailActionPerformed

    private void adicionaremailBTsalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionaremailBTsalvarActionPerformed
        if (funcao.equals("adicionar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).addEmail(adicionaremailETemail.getText());// adiciona ela no contato selecionado
            BancoDeDados.atualizarEmails(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }
        if (funcao.equals("editar")) {
            contatosList.get(JLcontatos.getSelectedIndex()).listaDeEmails.remove(JLemails.getSelectedIndex());
            contatosList.get(JLcontatos.getSelectedIndex()).addEmail(adicionaremailETemail.getText());// adiciona ela no contato selecionado
            BancoDeDados.atualizarEmails(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza no banco de dados
        }

        FrameAdicionarEmail.dispose();
        atualizarTela(contatosList.get(JLcontatos.getSelectedIndex()));//atualiza a tela
    }//GEN-LAST:event_adicionaremailBTsalvarActionPerformed

    private void JLemailsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_JLemailsValueChanged
        if (evt.getValueIsAdjusting()) {
            if ((JLcontatos.getSelectedIndex() != -1) && (JLemails.getSelectedIndex() != -1)) {
                BTeditarEmail.setEnabled(true);
                BTexcluirEmail.setEnabled(true);
            } else {
                BTeditarEmail.setEnabled(false);
                BTexcluirEmail.setEnabled(false);
            }
        }
    }//GEN-LAST:event_JLemailsValueChanged
//BOTOES DE EMAIL-----------------------------------------------------------------------------

//MÉTODOS E MAIN==================================================================================  
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                TelaPrincipal tela = new TelaPrincipal();
                tela.setVisible(true);
                tela.setResizable(false);
                tela.setTitle("Agenda Telefonica - MPrataSolutions");
            }
        });
    }

    public void atualizarTela(Contato contato) {
        LBnomeContato.setText(contato.nome);
        if (contato.grupo.equals("Amigo")) {
            LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_amigo.png")));
            LBgrupo.setText("Amigo");
            LBinfo1.setText("");
            LBinfo2.setText("");
        }
        if (contato.grupo.equals("Família")) {
            LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_familia.png")));
            LBgrupo.setText("Família");
            LBinfo1.setText("Parentesco: " + contato.parentesco);
            LBinfo2.setText("");
        }
        if (contato.grupo.equals("Trabalho")) {
            LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_trabalho.png")));
            LBgrupo.setText("Trabalho");
            LBinfo1.setText("Empresa: " + contato.empresa);
            LBinfo2.setText("Setor: " + contato.setor);
        }
        LBdataNascimento.setText(contato.dia + "/" + contato.mes + "/" + contato.ano);
        LBaniversario1.setText("Faltam");
        LBaniversario2.setText("para completar");
        LBaniversario3.setText("!!!");
        LBendereco1.setText(contato.rua + " - N° " + contato.numero + " - " + contato.bairro + " - " + contato.complemento);
        LBendereco2.setText(contato.cidade + " - " + contato.estado + " - " + contato.pais + " - " + contato.cep);

        //--------------------CALCULO DO ANIVERSÁRIO-------------------------------------//
        try {
            SimpleDateFormat dataFormater = new SimpleDateFormat("dd/MM/yyyy");
            Date dataAtual = new Date();
            Date dataNascimento = null;
            Date dataAniversario = null;

            dataNascimento = dataFormater.parse(contato.dia + "/" + contato.mes + "/" + contato.ano);
            dataAniversario = dataFormater.parse(contato.dia + "/" + contato.mes + "/2017");

            long idade = ((((((dataAtual.getTime() / 1000) / 60) / 60) / 24) / 30) / 12)
                    - ((((((dataNascimento.getTime() / 1000) / 60) / 60) / 24) / 30) / 12);

            if (dataAtual.compareTo(dataAniversario) == -1) {
                long contaDias = ((((dataAniversario.getTime() / 1000) / 60) / 60) / 24)
                        - ((((dataAtual.getTime() / 1000) / 60) / 60) / 24);
                LBcontaDias.setText(contaDias + " Dias");
                LBidade.setText(idade + " anos");

            } else {
                dataAniversario = dataFormater.parse(contato.dia + "/" + contato.mes + "/2018");
                long contaDias = ((((dataAniversario.getTime() / 1000) / 60) / 60) / 24)
                        - ((((dataAtual.getTime() / 1000) / 60) / 60) / 24);
                LBcontaDias.setText(contaDias + " Dias");
                LBidade.setText((idade + 1) + " anos");
            }
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        //FIM-------------------CALCULO DO ANIVERSÁRIO-----------------------------------//

        //----------------ATUALIZA LISTAS DE TELEFONE E REUNIÃO--------------------------//
        
            String telefones[] = new String[contato.listaDeTelefones.size()];
            for (int i = 0; i < contato.listaDeTelefones.size(); i++) {
                telefones[i] = contato.listaDeTelefones.get(i).operadora + " - ("
                        + contato.listaDeTelefones.get(i).codigo + ")"
                        + contato.listaDeTelefones.get(i).numero;
            }
            JLtelefones.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {
                    return telefones.length;
                }

                public String getElementAt(int i) {
                    return telefones[i];
                }
            });
        

        
            String reunioes[] = new String[contato.listaDeReunioes.size()];
            for (int i = 0; i < contato.listaDeReunioes.size(); i++) {
                reunioes[i] = contato.listaDeReunioes.get(i).titulo + " - ("
                        + //
                        contato.listaDeReunioes.get(i).data + ")";
            }
            JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {
                    return reunioes.length;
                }

                public String getElementAt(int i) {
                    return reunioes[i];
                }
            });
        

       
            String emails[] = new String[contato.listaDeEmails.size()];
            for (int i = 0; i < contato.listaDeEmails.size(); i++) {
                emails[i] = contato.listaDeEmails.get(i);
            }
            JLemails.setModel(new javax.swing.AbstractListModel<String>() {
                public int getSize() {
                    return emails.length;
                }

                public String getElementAt(int i) {
                    return emails[i];
                }
            });
        

        //FIM-------------ATUALIZA LISTAS DE TELEFONE E REUNIÃO--------------------------//
    }

    public void limpaTela() {
        LBnomeContato.setText("");
        LBgrupo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/agenda/telefonica/ic_amigo.png")));
        LBgrupo.setText("Amigo");
        LBinfo1.setText("");
        LBinfo2.setText("");
        LBdataNascimento.setText("");
        LBaniversario1.setText("");
        LBaniversario2.setText("");
        LBaniversario3.setText("");
        LBidade.setText("");
        LBcontaDias.setText("");
        LBendereco1.setText("");
        LBendereco2.setText("");

        String telefones[] = new String[0];
        JLtelefones.setModel(new javax.swing.AbstractListModel<String>() {
            public int getSize() {
                return telefones.length;
            }

            public String getElementAt(int i) {
                return telefones[i];
            }
        });
        String reunioes[] = new String[0];
        JLreunioes.setModel(new javax.swing.AbstractListModel<String>() {
            public int getSize() {
                return reunioes.length;
            }

            public String getElementAt(int i) {
                return reunioes[i];
            }
        });
        String emails[] = new String[0];
        JLemails.setModel(new javax.swing.AbstractListModel<String>() {
            public int getSize() {
                return emails.length;
            }

            public String getElementAt(int i) {
                return emails[i];
            }
        });

    }

    public void atualizaContatos() {
        contatosList = BancoDeDados.buscarContatos(CBmodoExibicao.getSelectedIndex());
        contatosString = new String[contatosList.size()];
        for (int i = 0; i < contatosList.size(); i++) {
            contatosString[i] = contatosList.get(i).nome;
        }
        JLcontatos.setModel(new AbstractListModel<String>() {
            public int getSize() {
                return contatosString.length;
            }

            public String getElementAt(int i) {
                return contatosString[i];
            }
        });
    }
//MÉTODOS E MAIN==================================================================================

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTadicionarContato;
    private javax.swing.JButton BTadicionarEmail;
    private javax.swing.JButton BTadicionarReuniao;
    private javax.swing.JButton BTadicionarTelefone;
    private javax.swing.JButton BTeditarContato;
    private javax.swing.JButton BTeditarEmail;
    private javax.swing.JButton BTeditarReuniao;
    private javax.swing.JButton BTeditarTelefone;
    private javax.swing.JButton BTexcluirContato;
    private javax.swing.JButton BTexcluirEmail;
    private javax.swing.JButton BTexcluirReuniao;
    private javax.swing.JButton BTexcluirTelefone;
    private javax.swing.JComboBox<String> CBmodoExibicao;
    private javax.swing.JTextField ETbusca;
    private javax.swing.JFrame FrameAdicionarContato;
    private javax.swing.JFrame FrameAdicionarEmail;
    private javax.swing.JFrame FrameAdicionarReuniao;
    private javax.swing.JFrame FrameAdicionarTelefone;
    private javax.swing.JList<String> JLcontatos;
    private javax.swing.JList<String> JLemails;
    private javax.swing.JList<String> JLreunioes;
    private javax.swing.JList<String> JLtelefones;
    private javax.swing.JLabel LBaddcontatoEmpresa;
    private javax.swing.JLabel LBaniversario1;
    private javax.swing.JLabel LBaniversario2;
    private javax.swing.JLabel LBaniversario3;
    private javax.swing.JLabel LBcontaDias;
    private javax.swing.JLabel LBdataNascimento;
    private javax.swing.JLabel LBendereco1;
    private javax.swing.JLabel LBendereco2;
    private javax.swing.JLabel LBgrupo;
    private javax.swing.JLabel LBidade;
    private javax.swing.JLabel LBinfo1;
    private javax.swing.JLabel LBinfo2;
    private javax.swing.JLabel LBnomeContato;
    private javax.swing.JButton addcontatoBTsalvar;
    private javax.swing.JComboBox<String> addcontatoCBano;
    private javax.swing.JComboBox<String> addcontatoCBdia;
    private javax.swing.JComboBox<String> addcontatoCBgrupo;
    private javax.swing.JComboBox<String> addcontatoCBmes;
    private javax.swing.JTextField addcontatoETbairro;
    private javax.swing.JTextField addcontatoETcep;
    private javax.swing.JTextField addcontatoETcidade;
    private javax.swing.JTextField addcontatoETcomplemento;
    private javax.swing.JTextField addcontatoETempresa_parentesco;
    private javax.swing.JTextField addcontatoETestado;
    private javax.swing.JTextField addcontatoETnome;
    private javax.swing.JTextField addcontatoETnumero;
    private javax.swing.JTextField addcontatoETpais;
    private javax.swing.JTextField addcontatoETrua;
    private javax.swing.JTextField addcontatoETsetor;
    private javax.swing.JButton addreuniaoBTsalvar;
    private javax.swing.JComboBox<String> addreuniaoCBano;
    private javax.swing.JComboBox<String> addreuniaoCBdia;
    private javax.swing.JComboBox<String> addreuniaoCBmes;
    private javax.swing.JTextArea addreuniaoETdescricao;
    private javax.swing.JTextField addreuniaoETtitulo;
    private javax.swing.JButton addtelefoneBTadicionar;
    private javax.swing.JComboBox<String> addtelefoneCBoperadora;
    private javax.swing.JTextField addtelefoneETcodigo;
    private javax.swing.JTextField addtelefoneETnumero;
    private javax.swing.JButton adicionaremailBTsalvar;
    private javax.swing.JTextField adicionaremailETemail;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    // End of variables declaration//GEN-END:variables
}
