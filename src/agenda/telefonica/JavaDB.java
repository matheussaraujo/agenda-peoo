package agenda.telefonica;
/**
 * @author Marcos Junior
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JavaDB {
    Connection con;
    JavaDB(){
        try{
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            con = DriverManager.getConnection("jdbc:derby:ContatosPEOO","usuario","usuario");
            System.out.println("Conectado ao Banco de Dados :) !!");
        }catch(ClassNotFoundException | SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    
    public void inserirContato(Contato contato){
        try{
            char grupo;
            switch (contato.grupo) {
                case "Trabalho":
                    grupo = 't';
                    break;
                case "Amigo":
                    grupo = 'a';
                    break;
                default:
                    grupo = 'f';
                    break;
            }
            JSONObject infoGrupo = new JSONObject();
            infoGrupo.put("parentesco",contato.parentesco);
            infoGrupo.put("empresa",contato.empresa);
            infoGrupo.put("setor",contato.setor);
            
            JSONObject endereco = new JSONObject();
            endereco.put("pais", contato.pais);
            endereco.put("estado", contato.estado);
            endereco.put("cidade", contato.cidade);
            endereco.put("bairro", contato.bairro);
            endereco.put("rua", contato.rua);
            endereco.put("numero", contato.numero);
            endereco.put("cep", contato.cep);
            endereco.put("complemento", contato.complemento);
            
            JSONObject nascimento = new JSONObject();
            nascimento.put("dia", contato.dia);
            nascimento.put("mes", contato.mes);
            nascimento.put("ano", contato.ano);
            
            
            JSONArray JlistaDeTelefones = new JSONArray();
            for(int i = 0; i< contato.listaDeTelefones.size(); i++){
                JSONObject telefone = new JSONObject();
                telefone.put("operadora", contato.listaDeTelefones.get(i).operadora);
                telefone.put("codigo", contato.listaDeTelefones.get(i).codigo);
                telefone.put("numero", contato.listaDeTelefones.get(i).numero);
                JlistaDeTelefones.add(telefone);
            }
            JSONObject telefones = new JSONObject();
            telefones.put("telefones",JlistaDeTelefones);
            
            JSONArray JlistaDeReunioes = new JSONArray();
            for(int i = 0; i< contato.listaDeReunioes.size(); i++){
                JSONObject reuniao = new JSONObject();
                reuniao.put("titulo", contato.listaDeReunioes.get(i).titulo);
                reuniao.put("data", contato.listaDeReunioes.get(i).data);
                reuniao.put("mensagem", contato.listaDeReunioes.get(i).mensagem);
                JlistaDeReunioes.add(reuniao);
            }
            JSONObject reunioes = new JSONObject();
            reunioes.put("reunioes", JlistaDeReunioes);
            
            JSONArray JlistaDeEmails = new JSONArray();
            for(int i = 0; i< contato.listaDeEmails.size(); i++){
                JlistaDeEmails.add(contato.listaDeEmails.get(i));
            }
            JSONObject emails = new JSONObject();
            emails.put("emails", JlistaDeEmails);
            
            Statement stm = con.createStatement();
            stm.execute("INSERT INTO CONTATOS VALUES("
                    + "'"+contato.nome+"',"
                    + "'"+grupo+"',"
                    + "'"+infoGrupo.toString()+"',"
                    + "'"+endereco.toString()+"',"
                    + "'"+nascimento.toString()+"',"
                    + "'"+telefones.toString()+"',"
                    + "'"+reunioes.toString()+"',"
                    + "'"+emails.toString()+"'" 
                    + ")");
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
        
    }
    public void atualizarContato(Contato contatoAntigo, Contato contatoNovo){
        excluirContato(contatoAntigo);
        inserirContato(contatoNovo);
    }
    public void excluirContato(Contato contato){
        try{
            Statement stm = con.createStatement();
            stm.execute("DELETE FROM CONTATOS WHERE NOME = '"+contato.nome+"'");
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    public List buscarContatos(int modoExibicao){
        String where = "";
        if(modoExibicao == 1){where = " WHERE GRUPO = 'f'";}
        if(modoExibicao == 2){where = " WHERE GRUPO = 'a'";}
        if(modoExibicao == 3){where = " WHERE GRUPO = 't'";}
        
        List<Contato> Contatos = new ArrayList<>();
        try{
            
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery("select * from CONTATOS"+ where +" order by NOME asc");
                      
            while(res.next()){
                Contato contato = new Contato();
                contato.nome = res.getString(1);
                String grupo = res.getString(2);
                switch(grupo){
                    case "t":
                        contato.grupo = "Trabalho";
                    break;
                    case "a":
                        contato.grupo = "Amigo";
                    break;
                    case "f":
                        contato.grupo = "Família";
                    break;
                }
                
                JSONParser parser = new JSONParser();
                JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                contato.parentesco = infoGrupo.get("parentesco").toString();
                contato.empresa = infoGrupo.get("empresa").toString();
                contato.setor = infoGrupo.get("setor").toString();
                
                JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                contato.pais = endereco.get("pais").toString();
                contato.estado = endereco.get("estado").toString();
                contato.cidade = endereco.get("cidade").toString();
                contato.bairro = endereco.get("bairro").toString();
                contato.rua = endereco.get("rua").toString();
                contato.numero = endereco.get("numero").toString();
                contato.cep = endereco.get("cep").toString();
                contato.complemento = endereco.get("complemento").toString();
                
                JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                contato.dia = nascimento.get("dia").toString();
                contato.mes = nascimento.get("mes").toString();
                contato.ano = nascimento.get("ano").toString();
                
                JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                for(int i = 0; i<listaDeTelefones.size(); i++){
                    JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                    Telefone tel = new Telefone(
                            telefone.get("operadora").toString(),
                            telefone.get("codigo").toString(),
                            telefone.get("numero").toString()
                    );
                    contato.listaDeTelefones.add(tel);
                }
                
                JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                for(int i = 0; i<listaDeReunioes.size(); i++){
                    JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                    Reuniao reuniaoPOJO = new Reuniao(
                            reuniao.get("titulo").toString(),
                            reuniao.get("data").toString(),
                            reuniao.get("mensagem").toString()
                    );
                    contato.listaDeReunioes.add(reuniaoPOJO);
                }
                
                JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                for(int i = 0; i<listaDeEmails.size(); i++){
                    contato.listaDeEmails.add(listaDeEmails.get(i).toString());
                }
             
                Contatos.add(contato);
            }
            
        }catch(SQLException  | ParseException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
        
        return Contatos;
    }
    public List pesquisarContatos(int modoExibicao, String pesquisa){
        String where = "";
        if(modoExibicao == 0){where = " WHERE NOME LIKE '%"+pesquisa+"%'";}
        if(modoExibicao == 1){where = " WHERE GRUPO = 'f' AND NOME LIKE '%"+pesquisa+"%'";}
        if(modoExibicao == 2){where = " WHERE GRUPO = 'a' AND NOME LIKE '%"+pesquisa+"%'";}
        if(modoExibicao == 3){where = " WHERE GRUPO = 't' AND NOME LIKE '%"+pesquisa+"%'";}
        
        List<Contato> Contatos = new ArrayList<>();
        try{
            
            Statement stm = con.createStatement();
            ResultSet res = stm.executeQuery("select * from CONTATOS"+ where +" order by NOME asc");
                      
            while(res.next()){
                Contato contato = new Contato();
                contato.nome = res.getString(1);
                String grupo = res.getString(2);
                switch(grupo){
                    case "t":
                        contato.grupo = "Trabalho";
                    break;
                    case "a":
                        contato.grupo = "Amigo";
                    break;
                    case "f":
                        contato.grupo = "Família";
                    break;
                }
                
                JSONParser parser = new JSONParser();
                JSONObject infoGrupo = (JSONObject) parser.parse(res.getString(3));
                contato.parentesco = infoGrupo.get("parentesco").toString();
                contato.empresa = infoGrupo.get("empresa").toString();
                contato.setor = infoGrupo.get("setor").toString();
                
                JSONObject endereco = (JSONObject) parser.parse(res.getString(4));
                contato.pais = endereco.get("pais").toString();
                contato.estado = endereco.get("estado").toString();
                contato.cidade = endereco.get("cidade").toString();
                contato.bairro = endereco.get("bairro").toString();
                contato.rua = endereco.get("rua").toString();
                contato.numero = endereco.get("numero").toString();
                contato.cep = endereco.get("cep").toString();
                contato.complemento = endereco.get("complemento").toString();
                
                JSONObject nascimento = (JSONObject) parser.parse(res.getString(5));
                contato.dia = nascimento.get("dia").toString();
                contato.mes = nascimento.get("mes").toString();
                contato.ano = nascimento.get("ano").toString();
                
                JSONObject telefones = (JSONObject) parser.parse(res.getString(6));
                JSONArray listaDeTelefones = (JSONArray) telefones.get("telefones");
                for(int i = 0; i<listaDeTelefones.size(); i++){
                    JSONObject telefone = (JSONObject) listaDeTelefones.get(i);
                    Telefone tel = new Telefone(
                            telefone.get("operadora").toString(),
                            telefone.get("codigo").toString(),
                            telefone.get("numero").toString()
                    );
                    contato.listaDeTelefones.add(tel);
                }
                
                JSONObject reunioes = (JSONObject) parser.parse(res.getString(7));
                JSONArray listaDeReunioes = (JSONArray) reunioes.get("reunioes");
                for(int i = 0; i<listaDeReunioes.size(); i++){
                    JSONObject reuniao = (JSONObject) listaDeReunioes.get(i);
                    Reuniao reuniaoPOJO = new Reuniao(
                            reuniao.get("titulo").toString(),
                            reuniao.get("data").toString(),
                            reuniao.get("mensagem").toString()
                    );
                    contato.listaDeReunioes.add(reuniaoPOJO);
                }              
                
                JSONObject emails = (JSONObject) parser.parse(res.getString(8));
                JSONArray listaDeEmails = (JSONArray) emails.get("emails");
                for(int i = 0; i<listaDeEmails.size(); i++){
                    contato.listaDeEmails.add(listaDeEmails.get(i).toString());
                }
             
                Contatos.add(contato);
            }
            
        }catch(SQLException  | ParseException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
        
        return Contatos;
    }
    
    public void atualizarTelefones(Contato contato){
        try{
            JSONArray JlistaDeTelefones = new JSONArray();  //ADICIONA OS QUE JA TINHA
            for(int i = 0; i< contato.listaDeTelefones.size(); i++){
                JSONObject telefoneJSON = new JSONObject();
                telefoneJSON.put("operadora", contato.listaDeTelefones.get(i).operadora);
                telefoneJSON.put("codigo", contato.listaDeTelefones.get(i).codigo);
                telefoneJSON.put("numero", contato.listaDeTelefones.get(i).numero);
                JlistaDeTelefones.add(telefoneJSON);
            }
            
            JSONObject telefones = new JSONObject();
            telefones.put("telefones",JlistaDeTelefones);
            
            PreparedStatement ps;
            String sql = "UPDATE CONTATOS SET TELEFONES =? WHERE NOME =?";
            ps = con.prepareStatement(sql);
            ps.setString(1, telefones.toString());
            ps.setString(2, contato.nome);
            ps.execute();
            ps.close();
            
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    public void atualizarReunioes(Contato contato){
        try{
            JSONArray JlistaDeReunioes = new JSONArray();  //ADICIONA OS QUE JA TINHA
            for(int i = 0; i< contato.listaDeReunioes.size(); i++){
                JSONObject reuniaoJSON = new JSONObject();
                reuniaoJSON.put("titulo", contato.listaDeReunioes.get(i).titulo);
                reuniaoJSON.put("data", contato.listaDeReunioes.get(i).data);
                reuniaoJSON.put("mensagem", contato.listaDeReunioes.get(i).mensagem);
                JlistaDeReunioes.add(reuniaoJSON);
            }
            
            JSONObject reunioes = new JSONObject();
            reunioes.put("reunioes",JlistaDeReunioes);
            
            PreparedStatement ps;
            String sql = "UPDATE CONTATOS SET REUNIOES =? WHERE NOME =?";
            ps = con.prepareStatement(sql);
            ps.setString(1, reunioes.toString());
            ps.setString(2, contato.nome);
            ps.execute();
            ps.close();
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
    public void atualizarEmails(Contato contato){
        try{
            JSONArray JlistaDeEmails = new JSONArray();
            for(int i = 0; i< contato.listaDeEmails.size(); i++){
                JlistaDeEmails.add(contato.listaDeEmails.get(i));
            }
            JSONObject emails = new JSONObject();
            emails.put("emails", JlistaDeEmails);
            
            PreparedStatement ps;
            String sql = "UPDATE CONTATOS SET EMAILS =? WHERE NOME =?";
            ps = con.prepareStatement(sql);
            ps.setString(1, emails.toString());
            ps.setString(2, contato.nome);
            ps.execute();
            ps.close();
        }catch(SQLException e){
            System.out.println("Banco de Dados: "+e.getMessage());
        }
    }
}
